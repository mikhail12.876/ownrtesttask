import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('trades')
export class Trade {
	@Column({type: "bigint", primary: true})
	id: bigint;

	@Column({ type: "bigint" })
	mts: bigint;

	@Column({ type: "float" })
	amount: string;

	@Column({ type: "numeric" })
	price: string;
}
