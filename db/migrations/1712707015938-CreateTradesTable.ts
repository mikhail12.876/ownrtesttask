import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTradesTable1712707015938 implements MigrationInterface {
    name = 'CreateTradesTable1712707015938'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "trades" ("id" bigint NOT NULL, "mts" bigint NOT NULL, "amount" double precision NOT NULL, "price" numeric NOT NULL, CONSTRAINT "PK_c6d7c36a837411ba5194dc58595" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "trades"`);
    }

}
