import * as path from "path";
import {DataSource} from "typeorm";
import { config } from 'dotenv';

config({
	path: process.env.NODE_ENV ? `.${process.env.NODE_ENV}.env` : '.env',
});


const connection = new DataSource({
	type: 'postgres',
	host: process.env.POSTGRES_HOST,
	port: Number(process.env.POSTGRES_PORT),
	username: process.env.POSTGRES_USER,
	password: process.env.POSTGRES_PASSWORD,
	database: process.env.POSTGRES_DB,
	synchronize: false,
	logging: true,
	entities: [
		path.join(__dirname, '/entities/*.entity.ts'),
	],
	migrations: [
		path.join(__dirname, '/migrations/*.ts'),
	],
	migrationsRun: true
});
export default connection;
