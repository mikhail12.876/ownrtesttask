import {Service, ServiceBroker} from "moleculer";
import {Trade} from "../db/entities/trade.entity";
import connection from "../db/data-source";
import WorkerService from "./worker.service";

export default class UserService extends Service {
	public constructor(broker: ServiceBroker) {

		super(broker);

		this.parseServiceSchema({
			name: "trades",
			actions: {
				getTrades: {
					rest: "GET /",
					async handler() {
						const tradeRepository = connection.getRepository(Trade);
						return tradeRepository.find();
					},
				},
			},
			routes: {
				"GET /trades": "getTrades"
			},
			settings: {
				fields: ["id", "mts", "amount", "price"],
			},
		});
	}
}
