import { Trade } from "../db/entities/trade.entity";
import { Service, ServiceBroker } from "moleculer";
import connection from "../db/data-source";

export default class WorkerService extends Service {
	private intervalId: NodeJS.Timeout | null = null;
	readonly tradeRepo = connection.getRepository(Trade);

	public constructor(broker: ServiceBroker) {
		super(broker);

		this.parseServiceSchema({
			name: "worker",
			actions: {
				async startFetchingTrades(): Promise<void> {
					if (!this.intervalId) {
						this.intervalId = setInterval(async () => {
							await this.fetchAndSaveTrades();
						}, 30000);
					}
				},

				async stopFetchingTrades(): Promise<void> {
					if (this.intervalId) {
						clearInterval(this.intervalId);
						this.intervalId = null;
					}
				}
			},
			started: async () => {
				await this.actions.startFetchingTrades();
			}
		});
	}

	private async fetchAndSaveTrades(): Promise<void> {
		try {
			const response = await fetch('https://api-pub.bitfinex.com/v2/trades/tBTCUSD/hist?limit=25');
			if (!response.ok) {
				throw new Error('Ошибка получения данных: ' + response.statusText);
			}

			const trades: Trade[] = await response.json();
			const transformedData: Trade[] = trades.map(item => ({
				id: item[0],
				mts: item[1],
				amount: item[2],
				price: item[3]
			}));

			await this.tradeRepo.upsert(transformedData, ['id']);
		} catch (error) {
			console.error('Ошибка при получении и сохранении торговых сделок:', error);
		}
	}
}
